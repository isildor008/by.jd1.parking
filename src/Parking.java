import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry on 24.05.2017.
 */
public class Parking {

   private List<Integer> parking = new ArrayList();
   private RandomUtil randomUtil=new RandomUtil();

    public int size() {
        return parking.size();

    }

    public int getById(int i) {

        return parking.get(i);
    }

    public void inicialization() {


            for (int i = 0; i < Constants.COUNT_PLACE; i++) {
                if(randomUtil.random().equals(false)){
                parking.add(0);}
                else {parking.add(1);
                }
            }


    }

    public void setInt(int index, int i) {
        parking.set(index, i);
    }

    @Override
    public String toString() {
        return "Parking{" +
                "parking=" + parking +
                '}';
    }
}
