
/**
 * Created by Dmitry on 24.05.2017.
 */
public class Auto implements Runnable {

    Parking parking;

    public Auto(Parking parking) {
        this.parking = parking;
    }

    @Override
    public void run() {

        int status = 0;

        for (int i = 0; i < parking.size(); i++) {

            long startTime = System.currentTimeMillis();
          synchronized (parking) {

                if (parking.getById(i) == 0) {
                    try {
                        Thread.sleep(Constants.MAX_SLEEP_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    parking.setInt(i, 1);
                    System.out.println();
                    status = 1;
                    break;

                }

                long endTime = System.currentTimeMillis() - startTime;
                if (endTime > Constants.MAX_PARKING_TIME) {
                    break;

              }

            }


        }
        if (status == 1) {
            System.out.println("Auto success parking");
        } else {
            System.out.println("False parking");
        }


    }
}
