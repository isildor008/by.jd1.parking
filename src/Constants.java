/**
 * Created by Dmitry on 24.05.2017.
 */
public class Constants {

    public static final int MAX_PARKING_TIME = 3000;
    public static final int MAX_SLEEP_TIME = 100;
    public static final int COUNT_PLACE = 10000000;
    public static final int COUNT_AUTO = 10000;

}
